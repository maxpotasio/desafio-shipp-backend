<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('country')->nullable(true);
            $table->string('license_number')->nullable(true);
            $table->string('operation_type')->nullable(true);
            $table->string('establishment_type')->nullable(true);
            $table->string('entity_name')->nullable(true);
            $table->string('dba_name')->nullable(true);
            $table->string('street_number')->nullable(true);
            $table->string('street_name')->nullable(true);
            $table->string('first_address')->nullable(true);
            $table->string('second_address')->nullable(true);
            $table->string('city')->nullable(true);
            $table->string('state')->nullable(true);
            $table->string('zip_code')->nullable(true);
            $table->string('square_footage')->nullable(true);
            $table->decimal('longitude', 10, 8)->nullable(true);
            $table->decimal('latitude', 10,8)->nullable(true);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
