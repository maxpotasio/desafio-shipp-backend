<?php
namespace App\Console\Commands;


use App\Store;

use Exception;
use Illuminate\Console\Command;

class UploadStores extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "upload:stores";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Upload stores from a csv";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $handle = fopen("database/csv/stores.csv", "r");
        $header = true;
        try{
            $this->info('> Iniciando upload de Lojas...');
            $bar = $this->output->createProgressBar(29390);
            $bar->start();

            while ($csvLine = fgetcsv($handle, 1000, ",")) {
                if ($header) {
                    $header = false;
                } else {
                    $location = $csvLine[14];
                    $location = str_replace("'",'"',$location);
                    $location = str_replace('"{','{',$location);
                    $location = str_replace('}"','}',$location);
                    $location = str_replace("False","false",$location);

                    if ($location) {
                        $location = json_decode($location);
                    }

                    $store = new Store();
                    $store->country = trim($csvLine[0]);
                    $store->license_number = trim($csvLine[1]);
                    $store->operation_type = trim($csvLine[2]);
                    $store->establishment_type = trim($csvLine[3]);
                    $store->entity_name = trim($csvLine[4]);
                    $store->dba_name = trim($csvLine[5]);
                    $store->street_number = trim($csvLine[6]);
                    $store->street_name = trim($csvLine[7]);
                    $store->first_address = trim($csvLine[8]);
                    $store->second_address = trim($csvLine[9]);
                    $store->city = trim($csvLine[10]);
                    $store->state = trim($csvLine[11]);
                    $store->zip_code = trim($csvLine[12]);
                    $store->square_footage = trim($csvLine[13]);
                    $store->longitude = floatval(isset($location->longitude) ? $location->longitude : 0);
                    $store->latitude = floatval(isset($location->latitude) ? $location->latitude : 0);

                    $store->save();
                }

                $bar->advance();
            }

            $bar->finish();
            $this->info('');
            $this->info('> Upload finalizado...');
        } catch(\Exception $e)
        {
            dd($e);
        }
    }
}
