<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StoreController extends Controller
{
    public function index(Request $request) {
        $listOfstores = Store::all();

        $closestStores = array();

        foreach ($listOfstores as $store)
        {
            $distance = $store->getDistance($request->latitude, $request->longitude);

            if ($distance >= 0 && $distance <= 6500.00) {
                $closestStores[] = ['store' => $store, 'distance' => $distance];
            }
        }

        if(!$closestStores == []){
            $countStores = count($closestStores);
            usort($closestStores, function($a, $b){
                if($a['distance'] >= $b['distance']){
                    return 1;
                }

                if($a['distance'] == $b['distance']){
                    return 0;
                }

                return -1;
            });
        } else {
            $closestStores = ['message'=>'Nenhuma loja encontrada.'];
            $countStores = 0;
        }

        $request->request->add(['countStores' => $countStores]);

        return response()->json($closestStores);
    }
}
