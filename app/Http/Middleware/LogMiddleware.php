<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }


    public function terminate($request, $response)
    {
        Log::info('>>>>>>>>>>>>> StoreMiddleware Request <<<<<<<<<');
        Log::info('Horário: ' . date('H:i:s'));
        Log::info('Latitude: ' . $request->latitude);
        Log::info('Longitude: ' . $request->longitude);
        Log::info('Status Code: '.$response->getStatusCode());
        Log::info('Number of stores: '. $request->countStores);
        Log::info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    }
}
