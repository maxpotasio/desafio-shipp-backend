<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class StoreMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!(isset($request->latitude) || isset($request->longitude))){
            return response()->json(['message' => 'Latitude e Longitude são necesárias.'], 400);
        }

        $validLat = preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', floatval($request->get('latitude')));
        $validLong = preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', floatval($request->get('longitude')));

        if (!($validLat && $validLong)) {
            return response()->json(['message' => 'Coordenadas invalidas.'], 400);
        }

        return $next($request);
    }
}
