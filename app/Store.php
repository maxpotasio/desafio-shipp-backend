<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Location\Coordinate;
use Location\Distance\Vincenty;

class Store extends Model {
    public function getDistance($lat, $long){
        $coordinate1 = new Coordinate($lat, $long);
        $coordinate2 = new Coordinate($this->latitude, $this->longitude);

        $calculator = new Vincenty();

        $distance = $calculator->getDistance($coordinate1, $coordinate2);

        return $this->numberFormatPrecision($distance, 2, '.');
    }

    function numberFormatPrecision($number, $precision = 2, $separator = '.') {
        $numberParts = explode($separator, $number);
        $response = $numberParts[0];
        if(count($numberParts)>1){
            $response .= $separator;
            $response .= substr($numberParts[1], 0, $precision);
        }
        return $response;
    }
}
